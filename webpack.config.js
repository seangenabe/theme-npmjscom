module.exports = {
  entry: "./src/index.ts",
  output: {
    path: `${__dirname}/public`,
    filename: "index.user.js"
  },
  resolve: {
    extensions: [".ts", ".js"]
  },
  module: {
    rules: [
      {
        test: /\.ts$/,
        exclude: /node_modules/,
        use: {
          loader: "babel-loader"
        }
      },
      {
        test: /\.css$/,
        use: ["style-loader", "css-loader"]
      }
    ]
  },
  plugins: [],
  externals: {},
  optimization: {}
}
