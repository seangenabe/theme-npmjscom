import debounce from "lodash.debounce"

const dRefresh = debounce(refresh, 200)

document.addEventListener("DOMContentLoaded", () => {
  const styleElement = createStyleElement()
  const mo = new MutationObserver(dRefresh)
  mo.observe(document.body, { attributes: true, subtree: true })
  refresh()

  document.body.appendChild(styleElement)
})

function refresh() {
  replaceThemeClasses()

  for (let path of $$(`g[fill="#0a0b09"] path`) as SVGPathElement[]) {
    path.style.fill = "#f5f6f4" // negate rotate
  }
}

function getMatchingClass(
  element: Element | undefined | null,
  classPredicate: (c: string) => boolean
) {
  if (!element) {
    return undefined
  }
  return Array.from(element.classList).find(classPredicate)
}

function createStyleElement() {
  const markdownClass =
    getMatchingClass(document.getElementById("readme"), s =>
      s.startsWith("markdown__")
    ) || "notfound"
  const footerClass =
    getMatchingClass(document.querySelector("main + footer"), s =>
      s.startsWith("footer__")
    ) || "notfound"
  const packageSectionHeaderClass =
    getMatchingClass(document.getElementById("user-content-keywords"), s =>
      s.startsWith("package__sectionHeader___")
    ) || "notfound"

  // To get the inverted luminosity of colors:
  // require('color').negate().rotate(180).hex()

  const rules: { [selector: string]: { [property: string]: string } } = {
    main: {
      color: "white"
    },
    ".markdown": {
      color: "#ccc"
    },
    ".markdown td, .markdown th": {
      background: "black"
    },
    ".markdown p, .markdown li": {
      color: "#ccc"
    },
    ".markdown tr:nth-child(2n) td": {
      "background-color": "#0b0b0b" // #f4f4f4
    },
    [[1, 2, 3, 4, 5, 6].map(h => `.${markdownClass} h${h}`).join(",")]: {
      color: "#eee"
    },
    [`.${markdownClass} h2`]: {
      "border-bottom-color": "rgba(255, 255, 255, 0.1)"
    },
    [`.${markdownClass} p`]: {
      color: "rgba(255, 255, 255, 0.85)"
    },
    [`.${markdownClass} strong`]: {
      color: "white"
    },
    [`.${markdownClass} code, .${markdownClass} pre:not(.editor-colors)`]: {
      background: "#080808" // white - #f7f7f7
    },
    [`.${markdownClass} ul li a, .${markdownClass} a, .marketing a, .flatpages p a, .flatpages .support a, .flatpages .home-page a, .flatpages .jobs a`]: {
      color: "#CB5353" // color("#cb3837").whiten(0.5)
    },
    ".marketing": {
      color: "#aaa",
      background: "rgba(255, 255, 255, 0.2)"
    },
    [`.${footerClass} h3, .${footerClass} a`]: {
      color: "rgba(228, 228, 228, 0.8)" // 255 - 27
    },
    [`.${packageSectionHeaderClass}`]: {
      color: "#eee"
    },
    "[class^='forms__buttonGradientPurple___'], [class^='forms__buttonGradientRed___']": {
      "background-color": "black"
    },
    "[class^='forms__buttonGradientPurple___'] .line, [class^='forms__buttonGradientPurple___'] .line-round": {
      fill: "white",
      stroke: "white"
    },
    "[class*='package__downloads___'] svg": {
      fill: "rgba(255, 255, 255, 0.45)"
    },
    "[class*='package__install___'] svg": {
      fill: "rgba(255, 255, 255, 0.5)"
    },
    "[class*='package__infobox']": {
      background: "#00070D"
    },
    "[class*='header__headerLinks']": {
      background: "black"
    },
    ".bg-black-05": {
      "background-color": "rgba(255, 255, 255, 0.05)"
    },
    "[class*='header__expansions'], [class*='header__firstPublish'], [class*='header__productNavLink']": {
      color: "#ddd"
    },
    "[class*='search__searchTopbarContainer']": {
      background: "#060606" // #f9f9f9
    },
    "[class*='pagination__page'] a": {
      "background-color": "black",
      color: "white",
      "border-color": "#303030" // #cfcfcf
    },
    "[class*='search__searchSidebar'] label": {
      color: "rgba(255, 255, 255, .7)"
    },
    "[class*='search__searchSidebar'] label:hover": {
      color: "white"
    },
    "[class*='package-list-item__metricLetter']": {
      color: "rgba(255, 255, 255, 0.9)"
    },
    "[class*='forms__buttonGradient']": {
      color: "white",
      "background-color": "black",
      "background-image":
        "linear-gradient(0deg, rgba(0, 0,0 , .09) 0%, rgba(238, 238, 238, .04) 100%)"
    },
    "[class*='forms__buttonGradient']:hover": {
      color: "#eee",
      "background-image":
        "linear-gradient(0deg, rgba(0,0,0, .18) 0%, rgba(238, 238, 238, .08) 100%)",
      border: "1px solid rgba(233, 233, 233, .20)"
    },

    ".flatpages p, .flatpages ol, .flatpages ul": {
      color: "#cccccc" // rgb(51, 51, 51)
    },
    ".flatpages": {
      color: "white"
    },
    ".flatpages .home-page .bg-npm-pastel-9": {
      "background-color": "#0F0F0F !important" // #f0f0f0
    },
    "[class*='liminal__container']": {
      "background-color": "#080808" // #F7F7F7
    },
    "[class*='forms__textInput']": {
      background: "black"
    },
    "[class*='forms__textInput']:focus": {
      "background-color": "black"
    },
    ".white-50": {
      color: "hsla(0,0%,0%,.5)"
    },
    ".black-50": {
      color: "hsla(0,0%,100%,.5)"
    },
    ".hover-black-80:focus, .hover-black-80:hover": {
      color: "rgba(255,255,255,.8)"
    },
    "[class*='header__userNameLink']": {
      color: "#080808"
    },
    ".b--white-10": {
      "border-color": "hsla(0,0%,0%,.1)"
    },
    ".b--black-10": {
      "border-color": "hsla(0,0%,100%,.1)"
    },
    "[class*='package__infoBox']": {
      background: "#00070D" // #f2f9ff
    },
    "[class*='forms__textInput']:disabled": {
      color: "rgba(255, 255, 255, .7)",
      "border-color": "rgba(255, 255, 255, .1)",
      "background-color": "rgba(255, 255, 255, .05)"
    },
    "tbody tr:nth-child(even)": {
      background: "#070707" // #f8f8f8
    }
  }

  const styleElement = document.createElement("style")
  styleElement.textContent = Object.entries(rules)
    .map(
      ([selector, declarations]) =>
        `${selector}{${Object.entries(declarations)
          .map(([property, value]) => `${property}:${value}`)
          .join(";")}}`
    )
    .join("")
  return styleElement
}

function replaceThemeClasses() {
  //document.body.classList.add("bg-black")
  document.body.style.backgroundColor = "black"
  replaceClass("black", "white")
  replaceClass("black-60", "white-60")
  replaceClass("black-70", "white-70")
  replaceClass("black-80", "white-80")
  replaceClass("black-90", "white-90")
  replaceClass("hover-black", "hover-white")
  replaceClass("bg-white", "bg-black")
}

function replaceClass(
  fromClass: string,
  toClass: string,
  sourceSelector?: string | (() => NodeListOf<any> | Iterable<Element>)
) {
  let nodes: NodeListOf<any> | Iterable<Element> | HTMLCollectionOf<Element>
  if (typeof sourceSelector === "function") {
    nodes = sourceSelector()
  } else if (sourceSelector) {
    nodes = document.querySelectorAll(sourceSelector)
  } else {
    nodes = document.getElementsByClassName(fromClass)
  }
  const nodes2 = Array.from(toIterable(nodes))
  for (let node of nodes2) {
    node.classList.remove(fromClass)
    node.classList.add(toClass)
  }
  return nodes2
}

function toIterable(
  nodes: NodeListOf<any> | Iterable<Node> | HTMLCollectionOf<Element>
): Iterable<Element> {
  if (nodes[Symbol.iterator]) {
    return nodes as Iterable<Element>
  } else {
    return Array.from(nodes)
  }
}

function $$(selector: string) {
  return Array.from(document.querySelectorAll(selector))
}
