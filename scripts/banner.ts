import { execSync } from "child_process"
const pkg = require("../package")

const r1 = execSync("git rev-list HEAD | wc -l", { encoding: "utf8" })
const r2 = execSync("git rev-parse HEAD", { encoding: "utf8" })
const gitRevisionCount = r1.trim()
const gitCommitHash = r2.trim()

function generateBanner() {
  return `// ==UserScript==
// @name         npmjs.com black theme
// @namespace    http://gitlab.com/seangenabe/theme-npmjscom
// @version      ${pkg.version}.${gitRevisionCount}.r${gitCommitHash.substring(
    0,
    7
  )}
// @description  ${pkg.description}
// @author
// @match        https://www.npmjs.com/*
// @grant        none
// @downloadURL  https://seangenabe.gitlab.io/theme-npmjscom/index.user.js
// @updateURL    https://seangenabe.gitlab.io/theme-npmjscom/index.user.js
// @run-at       document-start
// ==/UserScript==
`
}

const banner = generateBanner()

module.exports = banner

if (require.main === module) {
  const { writeFileSync, readFileSync } = require("fs")
  const filename = `${__dirname}/../public/index.user.js`
  const x = readFileSync(filename, { encoding: "utf8" })
  writeFileSync(filename, banner + x, { encoding: "utf8" })
}
