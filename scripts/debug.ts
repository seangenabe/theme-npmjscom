import Ppt = require("puppeteer-core")
import { Frame } from "puppeteer-core"
;(async () => {
  try {
    const browser = await Ppt.launch({
      executablePath:
        "C:\\Program Files (x86)\\Google\\Chrome\\Application\\chrome.exe",
      headless: false
    })
    const page = await browser.newPage()
    await page.goto("https://www.npmjs.com/package/ansi-colors", {
      timeout: 180000
    })
    await page.addScriptTag({
      path: `${__dirname}/../public/index.user.js`
    })
    page.on("framenavigated", (frame: Frame) => {
      frame.addScriptTag({
        path: `${__dirname}/../public/index.user.js`
      })
    })
    await new Promise(() => undefined)
  } catch (err) {
    console.error(err)
    process.exit(1)
  }
})()
